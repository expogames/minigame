﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class AuthenticationUser : MonoBehaviour
{
    public InputField userName, userMail; //Текстовые поля
    public Canvas pnlNoUser, pnlUserBar; // Канвасы с UI есть User или нет
    public Text error;
    private string usersData, userData;// usersData хранится список всех пользователей; userData записываются данные нового пользователя
    private bool newUser, statusEror; // Ключ новый или старый пользователь, есть ошибка ввода или нет
    private int userStatus; // Ключ авторизован пользователь или нет

    // Start is called before the first frame update
    void Start()
    {
        //PlayerPrefs.DeleteAll();
        if (PlayerPrefs.HasKey("usersData"))//Проверка на присутствие элемента в PlayerPrefs
        {
            usersData = PlayerPrefs.GetString("usersData");
        }
        if (PlayerPrefs.HasKey("userStatus"))
        {
            userStatus = PlayerPrefs.GetInt("userStatus");
        }
        if (userStatus == 1) //Проверка авторизован пользователь или нет. Включение разный канвасов
        {
            pnlNoUser.GetComponent<Canvas>().enabled = false;
            pnlUserBar.GetComponent<Canvas>().enabled = true;
        }
        else
        {
            pnlNoUser.GetComponent<Canvas>().enabled = true;
            pnlUserBar.GetComponent<Canvas>().enabled = false;
        }

    }

    public void btnLogin() //Кнопка входа
    {
        statusEror = false;
        string userNameData = userName.text;
        string userMailData = userMail.text;
        
        if (PlayerPrefs.HasKey("usersData"))
        {
            string[] loginData = usersData.Split(';');// Получем список всех пользователей
            for (int i = 0; i < loginData.Length; i++)//Перебираем полученный список
            {
                string[] data = loginData[i].Split(','); // Получаем данный i пользователя из общего списка
                if (data[0].ToString() == userNameData )// Сравниваем с введёнными данными
                {
                    if (data[1].ToString() == userMailData)
                    {
                        newUser = false;// Ставим что это старый пользователь
                        Debug.Log("Старый");
                        ProfilePlayer.nameUser = userNameData;// Записываем имя пользователя
                        pnlNoUser.GetComponent<Canvas>().enabled = false;// Переключаем интерфейс
                        pnlUserBar.GetComponent<Canvas>().enabled = true;
                        error.gameObject.SetActive(false);
                        break;
                    }
                    else
                    {
                        error.gameObject.SetActive(true);// Ошибка что такой userName уже используется
                        statusEror = true;
                        break;
                    }
                    
                }
                else
                {
                    newUser = true;
                }

            }
        }
        else
        {
            newUser = true;
        }
        if (newUser == true && statusEror!= true)// Создание нового пользователя
        {
            userData = userNameData + "," + userMailData; // Собираем данные в строку через разделитель
            usersData += userData + ";";// Добавляем в общий список пользователей
            PlayerPrefs.SetString("usersData", usersData);// Сохраняем список всех пользователей в playerPrefs
            Debug.Log("Новый");
            PlayerPrefs.SetString("userNameData", userNameData);// Сохраняем логин и почну нового пользователя
            PlayerPrefs.SetString("userMailData", userMailData);
            ProfilePlayer.nameUser = userNameData;// Записываем имя пользователя
            pnlNoUser.GetComponent<Canvas>().enabled = false;// Переключаем интерфейс
            pnlUserBar.GetComponent<Canvas>().enabled = true;
            error.gameObject.SetActive(false);
        }
        userStatus = 1; // Ставим статус авторизованного и сохраняем
        PlayerPrefs.SetInt("userStatus", userStatus);
    }

}
