﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ProfilePlayer : MonoBehaviour
{
    public static string nameUser;// Имя User
    public static int coinsValue;// количество монет
    public Text txtUser, txtCoins;// UI количества монет и имя user
    public GameObject pnlEnd;// Панель для плавного перехода при завершении
    public Canvas pnlNoUser, pnlUserBar;

    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey("userNameData"))//Проверка на присутствие элемента в PlayerPrefs
        {
            nameUser = PlayerPrefs.GetString("userNameData");
        }
        if (PlayerPrefs.HasKey("coinsValue"))
        {
            coinsValue = PlayerPrefs.GetInt("coinsValue");
        }
        txtCoins.text = coinsValue.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        txtUser.text = nameUser;
        txtCoins.text = coinsValue.ToString();
    }

    // кнопка играть
    public void btnPlay()
    {
        pnlEnd.gameObject.SetActive(true);
        SceneManager.LoadSceneAsync(1); //Загрузка другой сцены
    }

    //Стирание пользователя
    public void btnClear()
    {
        //PlayerPrefs.DeleteKey("usersData");  Если не удалять пользователя полностью
        PlayerPrefs.DeleteKey("userStatus");
        PlayerPrefs.DeleteKey("userNameData");
        PlayerPrefs.DeleteKey("userMailData");
        PlayerPrefs.DeleteKey("coinsValue");
        pnlNoUser.GetComponent<Canvas>().enabled = true;
        pnlUserBar.GetComponent<Canvas>().enabled = false;
        coinsValue = 0; // Обнуление монет
    }
}
