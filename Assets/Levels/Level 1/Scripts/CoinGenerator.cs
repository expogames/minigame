﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinGenerator : MonoBehaviour
{
    public GameObject arena, coin;
    public static int coinsCount;
    // Start is called before the first frame update
    void Start()
    {
        coinsCount = Random.Range(10, 20);
        for (int i = 0; i < coinsCount; i++)
        {
            Instantiate(coin, new Vector3(arena.GetComponentInChildren<Transform>().GetChild(Random.Range(0, arena.GetComponentInChildren<Transform>().childCount))
                .GetComponent<Transform>().transform.position.x, 0.2f, arena.GetComponentInChildren<Transform>().GetChild(Random.Range(0, arena.GetComponentInChildren<Transform>().childCount))
                .GetComponent<Transform>().transform.position.z), Quaternion.identity); // Получение позиции рандомного блока и спавн по его позиции монеты
            

        }

    }

}
