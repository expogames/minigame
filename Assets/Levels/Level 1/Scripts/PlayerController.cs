﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    // Закомичен старый вариант управления персонажем, т.к использоват ассет
    public GameObject Player;
    public Text txtCoins;
    public GameObject pnlEnd;
    int coins;
    public AudioSource soundCoin;// звук монеты
    //private float speedMove = 4f;//Скорость персонажа

    //private Vector3 moveVector;//Направление движения

    //private CharacterController ch_controller;
    //private Animator ch_animator;
    // Start is called before the first frame update
    void Start()
    {
       // ch_controller = GetComponent<CharacterController>();
        //ch_animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //CharacterMove();
        if (CoinGenerator.coinsCount == coins)
        {
            pnlEnd.gameObject.SetActive(true);// Плавность перехода
            ProfilePlayer.coinsValue = coins;
            PlayerPrefs.SetInt("coinsValue", coins);// Сохранение количества монет
            SceneManager.LoadSceneAsync(0);
        }

    }

    //private void CharacterMove()
    //{

    //    moveVector = Vector3.zero;
    //    moveVector.x = Input.GetAxis("Horizontal") * speedMove;
    //    moveVector.z = Input.GetAxis("Vertical") * speedMove;

    //    //Анимация передвижения
    //    if (moveVector.x != 0 || moveVector.z != 0)
    //    {
    //        ch_animator.SetBool("Move", true);
    //        speedMove = 4f;
    //    }
    //    else
    //    {
            
    //        ch_animator.SetBool("Move", false);
    //    }

    //    //Поворот в сторону движения
    //    if (Vector3.Angle(Vector3.forward, moveVector) > 1f || Vector3.Angle(Vector3.forward, moveVector) == 0)
    //    {
    //        Vector3 direct = Vector3.RotateTowards(transform.forward, moveVector, speedMove, 0.0f);
    //        transform.rotation = Quaternion.LookRotation(direct);
    //    }

    //    ch_controller.Move(moveVector * Time.deltaTime);
    //}
    // Сбор монет
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Coin"))// Проверка по тегу
        {
            other.gameObject.SetActive(false);
            coins++;
            soundCoin.GetComponent<AudioSource>().Play();
            txtCoins.text = coins.ToString();
        }

    }
}
